//Import thư viện mongoose
const mongoose = require("mongoose");

//tạo class schema lấy từ thư viện mongoose
const Schema = mongoose.Schema;

//tạo customerSchema từ class Schema
const customerSchema = new Schema ({
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        default: ""
    },
    city: {
        type: String,
        default: ""
    },
    country: {
        type: String,
        default: ""
    },
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: "Order"
    }]
})
//export customer model từ customerSchema
module.exports = mongoose.model("Customer", customerSchema);