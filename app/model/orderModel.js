//Khai báo thư viện mongoose
const mongoose = require ("mongoose");
//Khai báo thư viện schema
const Schema = mongoose.Schema;

//tạo orderSchema
const orderSchema = new Schema ({
    orderDate:{
        type: Date,
        default: Date.now()
    },
    shipperDate:{
        type: Date
    },
    note:{
        type: String
    },
    orderDetails: [{
        type: mongoose.Types.ObjectId,
        ref: "OrderDetail"
    }],
    cost: {
        type: Number,
        default: 0
    }
})

module.exports = mongoose.model("Order", orderSchema)