//Import thư viện mongoose
const mongoose = require("mongoose");

//tạo class schema lấy từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo productTypeSchema từ class Schema
const productTypeSchema = new Schema ({
    name: {
        type: String,
        unique: true,
        required: true,
    },
    description: {
        type: String
    }
});

//export product type model từ productTypeSchema
module.exports = mongoose.model("ProductType", productTypeSchema);