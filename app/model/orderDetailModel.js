//Khai báo thư viện mongoose
const mongoose = require("mongoose");
//Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Tạo order detail schema thông qua thư viện schema
const orderDetailSchema = new Schema ({
    product: {
        type: mongoose.Types.ObjectId,
        ref: "Product"
    },
    quantity : {
        type: Number,
        default: 0
    }
})

module.exports = mongoose.model("OrderDetail", orderDetailSchema);