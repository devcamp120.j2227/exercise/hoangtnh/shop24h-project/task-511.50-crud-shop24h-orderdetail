//Khai báo thư viện express js
const express = require("express");

//khai báo router
const router = express.Router();
//import order detail controller
const orderDetailController = require("../controllers/orderDetailController");

router.post("/orders/:orderId/orderDetails", orderDetailController.createOrderDetail);
router.get("/orders/:orderId/orderDetails", orderDetailController.getAllOrderDetailOfOrder);
router.get("/orderDetails/:orderDetailId", orderDetailController.getOrderDetailById);
router.put("/orderDetails/:orderDetailId", orderDetailController.updateOrderDetail);
router.delete("/orders/:orderId/orderDetails/:orderDetailId", orderDetailController.deleteOrderDetail);
module.exports = router;
