//Khai báo thư viện express js
const express = require("express");

//khai báo router app
const router = express.Router();

//Import Product Type controller
const productTypeController = require("../controllers/productTypeController");

router.post("/productTypes", productTypeController.createProductType);
router.get("/productTypes", productTypeController.getAllProductType);
router.get("/productTypes/:productTypeId", productTypeController.getProductTypeById);
router.put("/productTypes/:productTypeId", productTypeController.updateProductTypeById);
router.delete("/productTypes/:productTypeId", productTypeController.deleteProductTypeById);

module.exports = router;