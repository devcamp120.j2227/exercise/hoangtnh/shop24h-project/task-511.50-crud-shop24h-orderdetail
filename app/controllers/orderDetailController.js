//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Khai báo order detail model
const orderDetailModel = require("../model/orderDetailModel");
//import order model
const orderModel = require("../model/orderModel");

//function create new order detail
const createOrderDetail = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    const orderId = request.params.orderId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return response.status(400).json({
            status: "Bad request",
            message: "Order Id không hợp lệ"
        })
    }
    //B3: thao tác với database
    const newOrderDetail = {
        _id: mongoose.Types.ObjectId(),
        product: mongoose.Types.ObjectId(),
        quantity: body.quantity
    }
    orderDetailModel.create(newOrderDetail,(error, data) =>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //thêm Id của order detail mới được tạo vào mảng order được liên kêt qua ref Order detail
        orderModel.findByIdAndUpdate(orderId,{
            $push: {
                orderDetails: data._id
            }
        }, (err, updateOrder) =>{
            if(err){
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(201).json({
                status:"Created new Order Detail successfully",
                data: data
            })
        })
    })

}
//function get all order detail of order
const getAllOrderDetailOfOrder = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const orderId = request.params.orderId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return response.status(400).json({
            status: "Bad request",
            message: "Order Id không hợp lệ"
        })
    }
    //B3: xử lý database từ order model
    orderModel.findById(orderId)
            .populate("orderDetails")
            .exec((error, data) =>{
                if(error){
                    return response.status(500).json({
                        status:"Internal server error",
                        message: error.message
                    })
                }
                return response.status(200).json({
                    status:" Get Order Detail by order Id successfully",
                    data: data
                })
            })
}
//function get order detail by id
const getOrderDetailById = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const orderDetailId = request.params.orderDetailId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)){
        return response.status(400).json({
            status: "Bad request",
            message: "Order detail id không hợp lệ"
        })
    }
    //B3: thao tác với database
    orderDetailModel.findById(orderDetailId, (error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Get order detail successfully",
            data:data
        })
    })
}
//function update order detail by id
const updateOrderDetail = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const orderDetailId = request.params.orderDetailId;
    const body = request.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)){
        return response.status(400).json({
            status:" Bad request",
            message: " Order detail Id không hợp lệ"
        })
    }
    //B3: thao tác với database
    const updateOrderDetail = {};
    if(body.product !== undefined){
        updateOrderDetail.product = body.product
    }
    if(body.quantity !== undefined){
        updateOrderDetail.quantity = body.quantity
    }

    orderDetailModel.findByIdAndUpdate(orderDetailId, updateOrderDetail, (error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message:error.message
            })
        }
        return response.status(200).json({
            status:"Update order details successfully",
            data: data
        })
    })
}
//function delete by order detail id
const deleteOrderDetail = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const orderDetailId = request.params.orderDetailId;
    const orderId = request.params.orderId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return response.status(400).json({
            status: " Bad request",
            message: "order ID không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)){
        return response.status(400).json({
            status: "Bad request",
            message: "order detail id không hợp lệ"
        })
    }
    orderDetailModel.findByIdAndDelete(orderDetailId,(error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        orderModel.findByIdAndUpdate(orderId,{
            $pull: {orderDetails: orderDetailId}
        }, (err, updateOrder) => {
            if(err){
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(200).json({
                status: "Delete Order Detail successfully"
            })
        })
    })
}
module.exports = {
    createOrderDetail,
    getAllOrderDetailOfOrder,
    getOrderDetailById,
    updateOrderDetail,
    deleteOrderDetail
}